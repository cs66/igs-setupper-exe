﻿USE IGSscheduler;
/* Historical tables

DROP TABLE strip_trial
DROP TABLE strip_algo_event
DROP table strip_algo
DROP TABLE tray_trial
DROP TABLE tray;
DROP TABLE tray_algo_event
DROP TABLE tray_algo
DROP TABLE tower_env
DROP TABLE tower
DROP TABLE strip_trial_log
*/
IF OBJECT_ID('dbo.lift_queue_pending', 'V') IS NOT NULL 
  DROP VIEW lift_queue_pending;
IF OBJECT_ID('dbo.lift_queue_faf', 'U') IS NOT NULL 
  DROP TABLE lift_queue_faf;
IF OBJECT_ID('dbo.lift_queue_far', 'U') IS NOT NULL 
  DROP TABLE lift_queue_far;
IF OBJECT_ID('dbo.lift_history_dedup', 'V') IS NOT NULL 
  DROP VIEW lift_history_dedup;
IF OBJECT_ID('dbo.lift_history', 'U') IS NOT NULL 
  DROP TABLE lift_history;
IF OBJECT_ID('dbo.light_queue_pending', 'V') IS NOT NULL 
  DROP VIEW light_queue_pending;
IF OBJECT_ID('dbo.current_trial', 'V') IS NOT NULL 
  DROP VIEW current_trial;
IF OBJECT_ID('dbo.light_queue_faf', 'U') IS NOT NULL 
  DROP TABLE light_queue_faf;
IF OBJECT_ID('dbo.light_queue_far', 'U') IS NOT NULL 
  DROP TABLE light_queue_far;
IF OBJECT_ID('dbo.light_trial', 'U') IS NOT NULL 
  DROP TABLE light_trial;
IF OBJECT_ID('dbo.light_history', 'U') IS NOT NULL 
  DROP TABLE light_history;
IF OBJECT_ID('dbo.trial_image', 'U') IS NOT NULL 
  DROP TABLE trial_image;
IF OBJECT_ID('dbo.trial', 'U') IS NOT NULL 
  DROP table trial
IF OBJECT_ID('dbo.trial_log', 'U') IS NOT NULL 
DROP TABLE trial_log
IF OBJECT_ID('dbo.algo_event', 'U') IS NOT NULL 
DROP TABLE algo_event
IF OBJECT_ID('dbo.algo', 'U') IS NOT NULL 
DROP TABLE algo
IF OBJECT_ID('dbo.strip', 'U') IS NOT NULL 
DROP TABLE strip
IF OBJECT_ID('dbo.suc', 'U') IS NOT NULL 
DROP TABLE suc;
IF OBJECT_ID('dbo.camera', 'U') IS NOT NULL 
DROP TABLE camera;
IF OBJECT_ID('dbo.device', 'U') IS NOT NULL 
DROP TABLE device;
IF OBJECT_ID('dbo.session', 'U') IS NOT NULL 
DROP TABLE session;
IF OBJECT_ID('dbo.users', 'U') IS NOT NULL 
DROP TABLE users;
IF OBJECT_ID('dbo.strip_type_LED', 'U') IS NOT NULL 
DROP TABLE strip_type_LED;
IF OBJECT_ID('dbo.strip_type', 'U') IS NOT NULL 
DROP TABLE strip_type;
IF OBJECT_ID('dbo.spectrum', 'U') IS NOT NULL 
DROP TABLE spectrum;
IF OBJECT_ID('dbo.LED', 'U') IS NOT NULL 
DROP TABLE LED;
IF OBJECT_ID('dbo.poller', 'U') IS NOT NULL 
DROP TABLE poller;
IF OBJECT_ID('dbo.nums', 'U') IS NOT NULL 
DROP TABLE nums;
IF OBJECT_ID('dbo.HVAC_LOG', 'U') IS NOT NULL 
DROP TABLE HVAC_Log
IF OBJECT_ID('dbo.ENV_CUR', 'U') IS NOT NULL 
DROP TABLE ENV_CUR
IF OBJECT_ID('dbo.ENV_LOG', 'U') IS NOT NULL 
DROP TABLE ENV_LOG
GO

CREATE TABLE ENV_LOG(
   address VARCHAR(50),  -- IGS-INV01-TWR0001-TRY01-STP01,     SPI-SWE01-GWS0001-....  - IGS-INV01-AIR0001-....
   what VARCHAR(20), -- CO2 or HUM 
   value FLOAT,
   whn DATETIME,
   sysid VARCHAR(10),
   server_time DATETIME DEFAULT current_timestamp
   PRIMARY KEY (address,what,whn)
)
alter table env_log add bucket datetime
update ENV_LOG set bucket =  DATEADD(SECOND,FLOOR(DATEDIFF(SECOND,'1970-01-01',server_time)/900)*900,'1970-01-01') where bucket is null
create index bucketidx on env_log(bucket) include(value,server_time)
create index env_log_tdx on ENV_LOG(whn)
create index env_log_server_idx on env_log(server_time)


CREATE TABLE poller(
  id VARCHAR(64) PRIMARY KEY,
  lastRun DATETIME,
  thisRun DATETIME,
);

CREATE TABLE nums(i INT PRIMARY KEY);
INSERT INTO nums VALUES (1);
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
--1024 up to here
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
INSERT INTO nums SELECT i+m FROM nums JOIN (SELECT MAX(i) AS m FROM nums) AS m ON (1=1)
-- 1,000,000 or there abouts
CREATE TABLE users(
  name VARCHAR(50),
  role VARCHAR(50) DEFAULT 'observer',
  pwd BINARY(64),
  salt int,
  PRIMARY KEY(name)
);

INSERT INTO users(name,salt) VALUES ('andrew', FLOOR(power(2,30)*RAND()));
INSERT INTO users(name,salt) VALUES ('dave',   FLOOR(power(2,30)*RAND()));
INSERT INTO users(name,salt) VALUES ('greg',FLOOR(power(2,30)*RAND()));
INSERT INTO users(name,salt,role) VALUES ('niall',FLOOR(power(2,30)*RAND()),'administrator');
INSERT INTO users(name,salt) VALUES ('andris',FLOOR(power(2,30)*RAND()));
UPDATE users SET role='administrator' WHERE name='andrew' -- Can create users
UPDATE users SET role='scheduler' WHERE name IN ('dave','andris') -- Can create and schedule algos
UPDATE users
	SET pwd =hashbytes('SHA2_512',CONCAT(CONVERT(BINARY(64),'igs100100'),CONVERT(VARCHAR(64),salt)))
WHERE name IN ('andrew','dave','greg','niall','andris') --TODO remind andrew to change his password
INSERT INTO users(name,salt) VALUES ('matthew', FLOOR(power(2,30)*RAND()));
UPDATE users
	SET pwd =hashbytes('SHA2_512',CONCAT(CONVERT(BINARY(64),'igs100100'),CONVERT(VARCHAR(64),salt)))
WHERE name IN ('matthew') --TODO remind andrew to change his password
CREATE TABLE session(
	token INT PRIMARY KEY,
	[user] VARCHAR(50) FOREIGN KEY REFERENCES users(name),
	dateCreated DATETIME
);
CREATE TABLE LED(
  id VARCHAR(50) PRIMARY KEY,
  product_id VARCHAR(50)
);
INSERT INTO LED VALUES ('HyperRed','ORSRAM LH CPDP-3T4T-1');
INSERT INTO LED VALUES ('FarRed','ORSRAM GF CSHPM1.24-2S3S-1');
INSERT INTO LED VALUES ('TrueGreen','ORSRAM LT CPDP-KZLX-26');
INSERT INTO LED VALUES ('DeepBlue','ORSRAM CQDP-2U3U-W5-1-K');
INSERT INTO LED VALUES ('EQWhite','ORSRAM LUW CRDP-LTMP-MMMW-1');

CREATE TABLE strip_type(
  id VARCHAR(50) PRIMARY KEY
);
INSERT INTO strip_type VALUES ('Red-Green-Blue-FarRed');
INSERT INTO strip_type VALUES ('Red-FarRed-Blue-White');

CREATE TABLE strip_type_LED(
  strip_type VARCHAR(50) NOT NULL FOREIGN KEY REFERENCES strip_type(id),
  channel VARCHAR(50) NOT NULL,
  n INT NOT NULL DEFAULT 1,
  LED VARCHAR(50) NOT NULL FOREIGN KEY REFERENCES LED(id),
  PRIMARY KEY (strip_type,channel)
);
INSERT INTO strip_type_LED VALUES ('Red-Green-Blue-FarRed','RED',2,'HyperRed');
INSERT INTO strip_type_LED VALUES ('Red-Green-Blue-FarRed','GREEN',1,'TrueGreen');
INSERT INTO strip_type_LED VALUES ('Red-Green-Blue-FarRed','BLUE',1,'DeepBlue');
INSERT INTO strip_type_LED VALUES ('Red-Green-Blue-FarRed','WHITE',1,'FarRed');

INSERT INTO strip_type_LED VALUES ('Red-FarRed-Blue-White','RED',2,'HyperRed');
INSERT INTO strip_type_LED VALUES ('Red-FarRed-Blue-White','GREEN',1,'FarRed');
INSERT INTO strip_type_LED VALUES ('Red-FarRed-Blue-White','BLUE',1,'DeepBlue');
INSERT INTO strip_type_LED VALUES ('Red-FarRed-Blue-White','WHITE',1,'EQWhite');

CREATE TABLE spectrum(
  LED VARCHAR(50) NOT NULL FOREIGN KEY REFERENCES LED(id),
  nm FLOAT NOT NULL,
  iLevel INT DEFAULT 100 NOT NULL,
  mm FLOAT(53),
  PRIMARY KEY(LED,nm,iLevel)
);
/*
BLUE: [[450, 3, 400]], //Centre of the peak nm, height of the peak, spread of the peak
        'far red': [[730, 7, 400]],
        RED: [[660, 4, 400]],
        GREEN: [[530, 4, 200]],
        WHITE: [[440, 3.9, 200], [550, 4.8, 600]]
		*/

INSERT INTO spectrum(LED,nm,iLevel,mm)
SELECT 'DeepBlue', i, 100, 4.53 *  3 * exp(-square(i - 450)/400)
 FROM nums WHERE i BETWEEN 300 AND 750
INSERT INTO spectrum(LED,nm,iLevel,mm)
SELECT 'HyperRed', i, 100, 4.53 *  4 * exp(-square(i - 660)/400)
 FROM nums WHERE i BETWEEN 300 AND 750
INSERT INTO spectrum(LED,nm,iLevel,mm)
SELECT 'TrueGreen', i, 100, 4.53 *  4 * exp(-square(i - 530)/200)
 FROM nums WHERE i BETWEEN 300 AND 750
INSERT INTO spectrum(LED,nm,iLevel,mm)
SELECT 'EQWhite', i, 100,
	4.53 *  3.9 * exp(-square(i - 440)/200) +
	4.53 *  4.8 * exp(-square(i - 550)/600)
 FROM nums WHERE i BETWEEN 300 AND 750
INSERT INTO spectrum(LED,nm,iLevel,mm)
SELECT 'FarRed', i, 100, 4.53 *  4 * exp(-square(i - 730)/400)
 FROM nums WHERE i BETWEEN 300 AND 750


CREATE TABLE device(
  id INT PRIMARY KEY,
  kind VARCHAR(50) DEFAULT 'Tower',
  x INT DEFAULT 0,--position in facility, in mm
  y INT DEFAULT 0,
  z INT DEFAULT 0,
  w INT DEFAULT 4000,
  h INT DEFAULT 7200,
  d INT DEFAULT 4920,
  door_height INT DEFAULT 2000,
  orientation INT DEFAULT 0, -- rotate around centre in vertical axis
  outOfUse INT DEFAULT 0
);
--Here's the tower in Invergowrie
INSERT INTO device(id,kind,x,z,orientation,w,d,h) VALUES (1,'tower',0,2000,0,4000,4920,6250);
--Two trolleys in Sweden
INSERT INTO device(id,kind,x,z,orientation,  w,d,h) VALUES (100,'trolley 3 column',0,-2000,180,    565,1350,2000);
INSERT INTO device(id,kind,x,z,orientation,  w,d,h) VALUES (102,'trolley 3 column',2000,-2000,180, 565,1350,2000);
--Move the Swedish ones further away
update device set x = x+10000 where id in (100,102) and x<10000;
--One trolley in invergowrie
INSERT INTO device(id,kind,x,z,orientation,  w,d,h) VALUES (101,'trolley 3 column',2000,-2000,180, 565,1350,2000);

CREATE TABLE camera(
  id INT IDENTITY(1,1) PRIMARY KEY,
  device INT FOREIGN KEY REFERENCES device(id),
  posn VARCHAR(10),
  url VARCHAR(255),
  username VARCHAR(255),
  password VARCHAR(255)
);
INSERT INTO camera(device,posn,url,username,password) VALUES (1,'odd','http://192.168.240.211/Streaming/channels/1/picture','admin','Tornado100100');

CREATE TABLE suc(
  address VARCHAR(50) PRIMARY KEY,
  device INT FOREIGN KEY REFERENCES device(id) NOT NULL,
  x INT DEFAULT 0,--position in facility, in mm
  y INT DEFAULT 0,
  z INT DEFAULT 0,
  w INT DEFAULT 4000,
  h INT DEFAULT 25,
  d INT DEFAULT 2*820,
  outOfUse INT DEFAULT 0
);
--10 Trays in tower 1 - each is a suc
INSERT INTO suc(address,device,x,y,z)
  SELECT 'TWR001-'+FORMAT(i,'000'),1,0,FLOOR((i-1)/2)*250,(i % 2)*2*1640
    FROM nums WHERE i IN (3,5,7,9,11,13,15,17,19,21,23);

-- 8 Levels in trolley 100
INSERT INTO suc(address,device,x,y,z,  w,h,d)
  SELECT 'GWS100-'+FORMAT(i,'000'),100,0,(i-1)*250,0,  565,25,1350
   FROM nums WHERE i BETWEEN 1 AND 8

-- 8 Levels in trolley 102
INSERT INTO suc(address,device,x,y,z,   w,h,d)
  SELECT 'GWS102-'+FORMAT(i,'000'),102,0,(i-1)*250,0,  565,25,1350
   FROM nums WHERE i BETWEEN 1 AND 8

-- 8 Levels in trolley 101
DELETE FROM suc WHERE address LIKE 'GWS101%'
INSERT INTO suc(address,device,x,y,z,   w,h,d)
  SELECT 'GWS101-'+FORMAT(i,'000'),101,0,(i-1)*250,0,  565,25,1350
   FROM nums WHERE i BETWEEN 1 AND 8

CREATE TABLE strip(
  suc VARCHAR(50) NOT NULL,
  tower INT,
  tray INT,
  strip INT DEFAULT 0, -- 0 indicates only one strip on the tray - this is the broadcast event
  strip_type VARCHAR(50) DEFAULT 'Red-Green-Blue-FarRed' FOREIGN KEY REFERENCES strip_type(id),
  outOfUse INT DEFAULT 0,
  x INT, y INT, z INT,
  w INT DEFAULT 4000,
  d INT DEFAULT 1640,
  PRIMARY KEY (suc,strip),
  FOREIGN KEY (suc) REFERENCES suc(address)
);

DROP TABLE trolleyMap
--trolleyMap is just handy to deal with the odd wiring - can be deleted
CREATE TABLE trolleyMap(
  strip INT PRIMARY KEY,
  xu INT, -- In strip width units ie 0, 1 or 2
  yu INT, -- In strip height units 0,1,..7
  zu INT,
  level INT
);
DELETE FROM trolleyMap;
--Leftmost column
INSERT INTO trolleyMap SELECT 17+i-1, 0,(i-1), 0, i FROM nums WHERE i BETWEEN 1 AND 8;
--Rightmost column
INSERT INTO trolleyMap SELECT i,      2,(i-1), 0, i FROM nums WHERE i BETWEEN 1 AND 8;
--Middle column - numbered top to bottom
INSERT INTO trolleyMap SELECT 16-i+1, 1,(i-1), 0, i FROM nums WHERE i BETWEEN 1 AND 8;

INSERT INTO strip (suc,tower,tray,strip,strip_type)
SELECT 'GWS100-'+FORMAT(level,'000'),1,100,strip,'Red-Green-Blue-FarRed' FROM trolleyMap

INSERT INTO strip (suc,tower,tray,strip,strip_type)
SELECT 'GWS102-'+FORMAT(level,'000'),1,102,strip,'Red-FarRed-Blue-White' FROM trolleyMap

INSERT INTO strip (suc,tower,tray,strip,strip_type)
SELECT 'GWS101-'+FORMAT(level,'000'),1,101,strip,'Red-FarRed-Blue-White' FROM trolleyMap


-- Trays 1-49 have one strip each
INSERT INTO strip(suc,tower,tray,strip)
  SELECT address,device,RIGHT(address,3),0
    FROM suc
	WHERE address LIKE 'TWR001%';

CREATE TABLE algo(
    id INT PRIMARY KEY IDENTITY(1001,1),
    name VARCHAR(50),
    strip_type VARCHAR(50) DEFAULT 'Red-Green-Blue-FarRed' FOREIGN KEY REFERENCES strip_type(id),
	created_by VARCHAR(50) FOREIGN KEY REFERENCES users(name),
	created_when DATETIME DEFAULT CURRENT_TIMESTAMP,
	outOfUse INT DEFAULT 0,
	owner VARCHAR(50) FOREIGN KEY REFERENCES users(name)
);

CREATE TABLE algo_event(
    algo INT FOREIGN KEY REFERENCES algo(id),
	offset INT,
	kind VARCHAR(50) DEFAULT 'light',
	light_string VARCHAR(255),
	header VARCHAR(50),
	details VARCHAR(1000),
	val1 FLOAT,
	val2 FLOAT,
	qnty INT,
	PRIMARY KEY (algo,kind,offset)
);
INSERT INTO algo (name,strip_type) VALUES ('Green100','Red-Green-Blue-FarRed');
INSERT INTO algo_event(algo,offset,light_string)
  SELECT id,           0,'RED000000GREEN640064BLUE640064WHITE000000' FROM algo WHERE name='Green100';
INSERT INTO algo_event(algo,offset,light_string)
  SELECT id, 10*24*60*60,'RED000000GREEN000000BLUE000000WHITE000000' FROM algo WHERE name='Green100';
INSERT INTO algo (name,strip_type) VALUES ('White102','Red-FarRed-Blue-White');
INSERT INTO algo_event(algo,offset,light_string)
  SELECT id,           0,'RED000000GREEN000000BLUE000000WHITE640064' FROM algo WHERE name='White102';
INSERT INTO algo_event(algo,offset,light_string)
  SELECT id, 10*24*60*60,'RED000000GREEN000000BLUE000000WHITE000000' FROM algo WHERE name='White102';


INSERT INTO algo (name,strip_type,owner) VALUES ('BasilW01','Red-FarRed-Blue-White','andrew');
--Full red - then ramp up white to day 10, trail off after that
--Ramp up in days 0-8
INSERT INTO algo_event(algo,offset,light_string)
SELECT id, (i-1)*24*60*60/2,concat('RED640064GREEN000000BLUE000000WHITE',format(floor(100*(i-1)/16),'X2'),'0064')
FROM nums JOIN algo ON (name='BasilW01' and outOfUse=0)
WHERE i between 1 and 17
--Ramp down 9-15
INSERT INTO algo_event(algo,offset,light_string)
SELECT id, 8*24*60*60+(i-1)*24*60*60/2,concat('RED640064GREEN000000BLUE000000WHITE',format(floor(100*(17-i)/16),'X2'),'0064')
FROM nums JOIN algo ON (name='BasilW01' and outOfUse=0)
WHERE i between 2 and 17

INSERT INTO algo_event(algo,offset,kind,qnty,val1,val2)
  SELECT id, 2*24*60*60,'Water',20,100,100 FROM algo WHERE name='BasilW01';
INSERT INTO algo_event(algo,offset,kind,val1)
  SELECT id, 0*24*60*60,'Temperature',24.6 FROM algo WHERE name='BasilW01';
INSERT INTO algo_event(algo,offset,kind,val1)
  SELECT id, 9*60*60,'CO2',450 FROM algo WHERE name='BasilW01';
INSERT INTO algo_event(algo,offset,kind,val1)
  SELECT id, 4*24*60*60,'CO2',460 FROM algo WHERE name='BasilW01';
INSERT INTO algo_event(algo,offset,kind,header,details)
  SELECT id, 12*24*60*60,'Action','Inspection','Clean trays' FROM algo WHERE name='BasilW01';

-- Red-Green-Blue-FarRed
-- Red-FarRed-Blue-White
INSERT INTO algo (name,strip_type,owner) VALUES ('BasilW02','Red-FarRed-Blue-White','dave');
INSERT INTO algo_event(algo,offset,light_string)
  SELECT id,   0,'RED320064GREEN640064BLUE000000WHITE000000' FROM algo WHERE name='BasilW02';
INSERT INTO algo_event(algo,offset,light_string)
  SELECT id,  15*24*60*60 ,'RED000000GREEN000000BLUE000000WHITE000000' FROM algo WHERE name='BasilW02';
CREATE TABLE trial(
    id INT IDENTITY(1000,1) PRIMARY KEY,
    suc VARCHAR(50),
	startTime DATETIME,
	algo INT FOREIGN KEY REFERENCES algo(id),
	duration INT,
	crop VARCHAR(100),
	owner VARCHAR(50) FOREIGN KEY REFERENCES users(name),
	suspendUntil DATETIME DEFAULT '1970-01-01',
	FOREIGN KEY (suc) REFERENCES suc(address),
);
CREATE INDEX trial_suc_start ON trial(suc,startTime);
GO
CREATE VIEW current_trial AS
	SELECT t.id,t.suc,t.startTime, t.algo, t.duration, t.crop, t.owner, t.suspendUntil
	  FROM
	   (SELECT suc,MAX(startTime) startTime
		 FROM trial
		  WHERE CURRENT_TIMESTAMP BETWEEN startTime AND DATEADD(SECOND,duration,startTime)
		  GROUP BY suc) AS a JOIN trial AS t ON a.suc=t.suc AND a.startTime=t.startTime
GO

CREATE TABLE trial_image(
   id INT IDENTITY(9000,1) PRIMARY KEY,
   trial INT FOREIGN KEY REFERENCES trial(id),
   url VARCHAR(250),
   notes TEXT NOT NULL DEFAULT '',
   whn DATETIME DEFAULT CURRENT_TIMESTAMP
)
CREATE INDEX trial_image_trial ON trial_image(trial);

--light queue fire and forget
CREATE TABLE light_queue_faf(
   seq INT IDENTITY(2001,1) PRIMARY KEY,
   lightstring VARCHAR(100),
   ttg DATETIME,
   trial INT FOREIGN KEY REFERENCES trial(id)
);
CREATE TABLE light_history(
   id INT IDENTITY(2001,1) PRIMARY KEY,
   seq INT,
   lightstring VARCHAR(100),
   response VARCHAR(100)
);
CREATE INDEX lhs ON light_history(seq);
--light_queue_faf gets deleted - light queue fire and remember
CREATE TABLE light_queue_far(
  seq INT PRIMARY KEY,
  lightstring VARCHAR(100),
  ttg DATETIME,
  trial INT,
  suc VARCHAR(50)
);
CREATE INDEX far_suc_seq ON light_queue_far(suc,seq);
CREATE INDEX far_suc_ttg ON light_queue_far(suc,ttg);
GO
CREATE VIEW light_queue_pending AS
  SELECT seq,lightstring
    FROM light_queue_faf LEFT JOIN trial ON trial=trial.id
   WHERE ttg<CURRENT_TIMESTAMP AND COALESCE(suspendUntil,'1970-01-01')<CURRENT_TIMESTAMP
GO

CREATE TABLE lift_queue_faf(
   seq INT IDENTITY(3001,1) PRIMARY KEY,
   ttg DATETIME,
   command VARCHAR(10), -- MOVE or WATER
   shaft INT, --or tower number
   tray INT,
   offset INT, --Only valid for a MOVE
   litres INT, --Only valid for WATER
   reservoir INT,--Only valid for WATER - indicates which tank to draw from
   trial INT FOREIGN KEY REFERENCES trial(id)
);
CREATE TABLE lift_queue_far(
   seq INT PRIMARY KEY,
   ttg DATETIME,
   command VARCHAR(10), -- MOVE or WATER
   shaft INT, --or tower number
   tray INT,
   offset INT, --Only valid for a MOVE
   litres INT, --Only valid for WATER
   reservoir INT,--Only valid for WATER - indicates which tank to draw from
   trial INT FOREIGN KEY REFERENCES trial(id),
   suc VARCHAR(50)
);
CREATE TABLE lift_history(
   id INT IDENTITY(3000,1) PRIMARY KEY,
   seq INT,
   error VARCHAR(30),
   whn DATETIME DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX lfths ON lift_history(seq);
CREATE INDEX lfthsw ON lift_history(seq,whn)
GO
CREATE VIEW lift_queue_pending AS
  SELECT seq, command, shaft, tray, offset,litres, reservoir
    FROM lift_queue_faf LEFT JOIN trial ON trial=trial.id
  WHERE ttg<current_timestamp  AND COALESCE(suspendUntil,'1970-01-01')<CURRENT_TIMESTAMP
GO
CREATE VIEW lift_history_dedup AS
	SELECT h.id,h.seq,h.error,h.whn
	FROM lift_history h JOIN
	 (select seq,MAX(id) as id
		from lift_history
	   group by seq) as x ON h.seq=x.seq AND h.id=x.id
GO

--Historical record - no FKs
CREATE TABLE trial_log(
    suc VARCHAR(50),
	startTime DATETIME,
	algo INT,
	duration INT,
	crop VARCHAR(100),
	owner VARCHAR(50),
	PRIMARY KEY(suc,startTime)
);
GO
IF OBJECT_ID('dbo.trigger_AddHistory', 'TR') IS NOT NULL 
  DROP TRIGGER trigger_AddHistory
GO
CREATE TRIGGER trigger_AddHistory ON trial
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DELETE FROM trial_log
	  WHERE suc+CONVERT(VARCHAR(22),startTime,126) IN
	    (SELECT suc+CONVERT(VARCHAR(22),startTime,126)  FROM INSERTED)
    INSERT INTO
		trial_log(suc, startTime, algo, duration, crop, owner)
	SELECT
		suc, startTime, algo, duration, crop, owner
	FROM
		INSERTED
END
GO
